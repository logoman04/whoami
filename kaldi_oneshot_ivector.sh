#test_recordings contains one_shot recordings
#this script writes the wav.scp and utt2spk files necessary for feature extraction
whoami_prepare_data.sh test_recordings/

#calculates mfcc features to be used to extract ivectors
compute-mfcc-feats --sample-frequency=8000 scp:data/test/wav.scp ark,scp:data/test/feats.ark,data/test/feats.scp

utils/fix_data_dir.sh data/test

sid/compute_vad_decision.sh --nj 1 data/test exp/test data/test

#extracts ivectors using the previously trained ivector extractor
sid/extract_ivectors.sh --nj 1 exp/extractor data/test exp/test

# ivectors contained at exp/test/ivector.1.ark. Can then be accessed through python kaldi io