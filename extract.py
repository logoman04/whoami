import os
from python_speech_features import mfcc
import scipy.io.wavfile as wav

directory = '/Users/loganford16/Documents/6.345/final_project_data/swb1/wav_files'

def extract_mfcc(filename):
    (rate,sig) = wav.read(filename)
    mfcc_feat = mfcc(sig,rate)
    return mfcc_feat

for f in os.listdir(directory):
    mfcc = extract_mfcc(directory + '/' + f)