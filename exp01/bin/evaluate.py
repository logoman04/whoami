#!/Users/loganford16/anaconda2/envs/bob_py27/bin/python -S
# Automatically generated on Mon May 15 09:12:08 2017

'''Runs a specific user program'''


import sys
sys.path[0:0] = [
  '/Users/loganford16/Documents/whoami/exp01/eggs/bob.db.voxforge-2.1.2-py2.7.egg',
  ]
if sys.version_info[:2] >= (3, 6): #see: http://bugs.python.org/issue30167
  _hack = str(sys.modules['__main__'].__loader__.__module__)
  sys.modules['__main__'].__loader__.__module__ += '_'
import site #initializes site properly
site.main() #this is required for python>=3.4
if sys.version_info[:2] >= (3, 6): #restore original value just in case...
  sys.modules['__main__'].__loader__.__module__ = _hack
import pkg_resources #initializes virtualenvs properly

import bob.bio.base.script.evaluate

if __name__ == '__main__':
    sys.exit(bob.bio.base.script.evaluate.main())
