MANIFEST.in
README.rst
buildout.cfg
setup.cfg
setup.py
version.txt
bob/__init__.py
bob.buildout.egg-info/PKG-INFO
bob.buildout.egg-info/SOURCES.txt
bob.buildout.egg-info/dependency_links.txt
bob.buildout.egg-info/entry_points.txt
bob.buildout.egg-info/not-zip-safe
bob.buildout.egg-info/requires.txt
bob.buildout.egg-info/top_level.txt
bob/buildout/__init__.py
bob/buildout/develop.py
bob/buildout/envwrapper.py
bob/buildout/extension.py
bob/buildout/gdbpy.py
bob/buildout/python.py
bob/buildout/script.py
bob/buildout/scripts.py
bob/buildout/test_envwrapper.py
bob/buildout/tools.py