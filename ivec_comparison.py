import os
import scipy.spatial.distance as ssd
import kaldi_io as kio

ivec_file = "./ivectors/ivector.1.ark"

ivecs = {key:vec for key,vec in kio.read_vec_flt_ark(ivec_file)}
key_ivec = "loganM_00001_00001"
iva = ivecs.get(key_ivec)
minDist = [1, ""]
for key in ivecs:
	# get IVecs from file
	if key == key_ivec:
		continue
	ivec = ivecs[key]
	cosd = ssd.cosine(iva, ivec)
	if cosd < minDist[0]:
		minDist = [cosd, key]
print minDist[1]