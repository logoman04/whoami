#!/usr/bin/python
""" Python Audio Recorder """

import __future__
import sys
import pyaudio
import wave
try:
	import Tkinter as tk
except ImportError:
	import tkinter as tk
 

''' keyboard listener'''
 
try:
    from msvcrt import getch  # try to import Windows version
except ImportError:
    def getch():   # define non-Windows version
        import tty, termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

''' Audio Recorder '''

frames = []
record_on = False

def cb(in_data, frame_count, time_info, status):
	if record_on:
		global frames
		frames.append(in_data)

	callback_flag = pyaudio.paContinue

	return in_data, callback_flag

def recordAudio(filename):
	print("** Start recordAudio")
	global record_on
	CHUNK = 1024
	FORMAT = pyaudio.paInt16
	CHANNELS = 2
	RATE = 8000
	'''
	print("* Press space to begin recording")
	while(record_on == False):
		char = getch()
		if char is not None:
			if char.decode('utf-8') == ' ':
				record_on = True
				char = None

	print("* Recording. Press space again to end recording")
	'''
	print("* Recording.")
	p = pyaudio.PyAudio()
	stream = p.open(format=FORMAT,
					channels=CHANNELS,
					rate=RATE,
					input=True,
					frames_per_buffer=CHUNK,
					stream_callback=cb)
	stream.start_stream()
	'''
	while(record_on) :
		char = getch()
		if char is not None:
			if char.decode('utf-8') == ' ':
				record_on = False
				char = None
	'''
	while(record_on) :
		pass
	print("* Done recording.")
	stream.stop_stream()
	stream.close()
	p.terminate()

	global frames
	wf = wave.open(filename, 'wb')
	wf.setnchannels(CHANNELS)
	wf.setsampwidth(p.get_sample_size(FORMAT))
	wf.setframerate(RATE)
	wf.writeframes(b''.join(frames))
	wf.close()
	print("* Recording saved to: " + filename)
	frames = []

CHUNK = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 2
RATE = 8000

p = None
stream = None

def startStream():
	global p, stream, RATE, CHUNK, FORMAT
	p = pyaudio.PyAudio()
	stream = p.open(format=FORMAT,
					channels=CHANNELS,
					rate=RATE,
					input=True,
					frames_per_buffer=CHUNK,
					stream_callback=cb)
	stream.start_stream()

def stopStream():
	global p, stream
	stream.stop_stream()
	stream.close()
	p.terminate()

def startRecording():
	global record_on
	record_on = True
	print("* Recording started")

def stopRecording():
	global record_on
	record_on = False
	print("* Recording stopped")

def saveAudio(filename):
	global frames, record_on, CHANNELS, RATE, FORMAT, p
	record_on = False
	wf = wave.open(filename, 'wb')
	wf.setnchannels(CHANNELS)
	wf.setsampwidth(p.get_sample_size(FORMAT))
	wf.setframerate(RATE)
	wf.writeframes(b''.join(frames))
	wf.close()
	print("* Recording saved to: " + filename)
	frames = []
	#print("* Frames cleared")


''' Application Interface '''

tkframes = []
rf = None
usernameEntry = None
dialog = None
rec = None
blurb = None
folder_prefix = "Sample_Recordings/"
enrolledlst = None

training_text = 'Hello there! My name is <your name>, and this is my "Who Am I" enrollment recording. I hope this system will be able to recognize me later!'

def startUserReg():
	global usernameEntry, rf
	rf = tk.Toplevel()
	rf.title("Add new User")
	unlbl = tk.Label(rf, text="Name")
	unlbl.pack({"side":"left"})
	usernameEntry = tk.Entry(rf)
	usernameEntry.pack({"side":"left"})
	submit = tk.Button(rf, text="Next")
	submit["command"] = trainUser
	submit.pack({"side":"right"})

def trainUser():
	global usernameEntry, rf, training_text, rec, blurb
	uneText = usernameEntry.get()
	rf.destroy()
	rf = tk.Toplevel()
	rf.title("Add new User")
	blurb = tk.Text(rf)
	blurb.insert(tk.INSERT, training_text)
	blurb.pack()
	rec = tk.Button(rf, text="Record", command = lambda: recordNewUser(uneText))
	rec.pack()

def recordNewUser(username):
	global record_on, rf, folder_prefix, rec, blurb
	if(record_on):
		stopRecording()
		un = username.replace(" ", "_")
		fn = folder_prefix + un+ "_train.wav"
		saveAudio(fn)
		rf.destroy()
		createNewUser(username)
	else:
		startRecording()
		rec["text"] = "Stop"
		blurb.insert(tk.INSERT, "\n\nRecording...")

def createNewUser(username):
	# do iVector stuff
	addUserToDir(username)
	addLineToDialog(username + ", you've just enrolled. " + "\n" 
					+ "Hit push to talk to see if I can recognize you!")
	updateUserList()
	return

def addUserToDir(username):
	with open('enrolled/enrolled_users.txt', 'a') as file:
		file.write(username + '\n')

def addLineToDialog(line):
	global dialog
	nl = "\n"
	dialog.insert(tk.INSERT, line + nl + nl)

def pushToTalk():
	global record_on
	if(record_on):
		stopRecording()
		saveAudio("test.wav")
		# test sound
		runTest()
	else:
		startRecording()

def getFilenameForUser(user, ext):
	fn = user.replace(" ", "_") + ext
	return fn.toLower()

def getEnrolledUsers():
	txt = open("enrolled/enrolled_users.txt")
	users = []
	for line in txt:
		#filename = getFilenameForUser(line, ".wav")
		users.append(line)
	return users

def updateUserList():
	global enrolledlst
	enrolledlst.delete(0, enrolledlst.size())
	users = getEnrolledUsers()
	for i in range(len(users)):
		enrolledlst.insert(i, users[i])

def makeiVector(wavFile):
	return []

def getiVector(username):
	return []

def findiVectorMatch(iv):
	match = [1, None]
	users = getEnrolledUsers()
	for user in users:
		uiv = getiVector(user)
		# cosine distance between the two
		dist = 0.5
		if dist < match[0]:
			match = [dist,user]
	# test vs UBM

def runTest():
	addLineToDialog("Trying to figure out who you are...")
	iv = makeiVector("test.wav")
	match = findiVectorMatch(iv)


startStream()
root = tk.Tk()
root.title("Who Am I")
content = tk.Frame(root, width=800, height=600)
#frame = tk.Frame(content, width=800, height=600)
leftpane = tk.Frame(content, borderwidth=5, relief="sunken", width=600, height=600)
rightpane = tk.Frame(content, borderwidth=5, relief="sunken", width=200, height=600)

# Left Pane items
p2t = tk.Button(leftpane, text="Push To Talk")
p2t["command"] = pushToTalk

whoamilbl = tk.Label(leftpane, text="Who Am I - Speaker ID")

dialog = tk.Text(leftpane)
addLineToDialog('Welcome to -- Who Am I -- Speaker ID!')
addLineToDialog('Click "Add User" in the bottom-right corner to enroll, ' + '\n'
			  + 'or click "Push To Talk" to see if I can recognize you!')

# Right Pane items
enrolledlbl = tk.Label(rightpane, text="Enrolled Users:")

enrolledlst = tk.Listbox(rightpane)
users = getEnrolledUsers()
for i in range(len(users)):
	enrolledlst.insert(i, users[i])

addUser = tk.Button(rightpane, text="Add User")
addUser["command"] = startUserReg

# Alignment

content.grid(column=0, row=0, columnspan=8, rowspan=6, sticky="nsew")
#frame.grid(column=0, row=0, columnspan=8, rowspan=6)
leftpane.grid(column=0, row=0, columnspan=5, rowspan=6, sticky="sw" )
rightpane.grid(column=5, row=0, columnspan=3, rowspan=6, sticky="se")

whoamilbl.grid(column=0, row=0, rowspan=1, sticky="nw")
dialog.grid(column=0, row=1, columnspan=5, rowspan=3, sticky="e")
p2t.grid(column=0, row=5, sticky="sw")#, columnspan=5, rowspan=1)

enrolledlbl.grid(column=5, row=0, rowspan=1, sticky="ne")
enrolledlst.grid(column=5, row=1, columnspan=3, rowspan=3, sticky="e")
addUser.grid(column=5, row=5, sticky="se")

#row / col configure
root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)
content.columnconfigure(0, weight=1)
content.columnconfigure(1, weight=1)
content.columnconfigure(2, weight=1)
content.columnconfigure(3, weight=1)
content.columnconfigure(4, weight=1)
content.columnconfigure(5, weight=1)
content.columnconfigure(6, weight=1)
content.columnconfigure(7, weight=1)

content.rowconfigure(0, weight=1)
content.rowconfigure(1, weight=1)
content.rowconfigure(2, weight=1)
content.rowconfigure(3, weight=1)
content.rowconfigure(4, weight=1)
content.rowconfigure(5, weight=1)

# start app
root.mainloop()

root.destroy()
stopStream()

#recordAudio("test2.wav")






